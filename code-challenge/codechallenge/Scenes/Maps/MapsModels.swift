//
//  MapsModels.swift
//  code-challenge
//
//  Created by mahesh on 3/31/18.
//  Copyright (c) 2018 mahesh. All rights reserved.
//
//  This file was generated by the Clean Swift HELM Xcode Templates
//  https://github.com/HelmMobile/clean-swift-templates
//
//  Type "usecase" for some magic!
import MapKit
import CoreData
struct MapsScene {
    class Annotation: NSObject, MKAnnotation {
        var name:String
        var coordinate: CLLocationCoordinate2D
        var location: CLLocation
        var title:String?
        var lat:Double
        var lng:Double
        init(location:Locations?) {
            self.title = (location?.formatted_address!)!
            self.name = (location?.formatted_address!)!
            self.lng = (location?.lng)!
            self.lat = (location?.lat)!
            self.coordinate = CLLocationCoordinate2D(latitude:(location?.lat)!, longitude: (location?.lng)!)
            self.location = CLLocation(latitude:(location?.lat)!, longitude: (location?.lng)!)
            super.init()
        }
        var subtitle: String? {
            return "\(self.lat) \(self.lng)"
        }
    }
}
