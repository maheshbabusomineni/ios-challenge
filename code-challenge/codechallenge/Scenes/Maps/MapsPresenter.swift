//
//  MapsPresenter.swift
//  code-challenge
//
//  Created by mahesh on 3/31/18.
//  Copyright (c) 2018 mahesh. All rights reserved.
//
//  This file was generated by the Clean Swift HELM Xcode Templates
//  https://github.com/HelmMobile/clean-swift-templates

protocol MapsPresenterInput {
    
}

protocol MapsPresenterOutput: class {
    
}

class MapsPresenter: MapsPresenterInput {
    
    weak var output: MapsPresenterOutput?
    
    // MARK: Presentation logic
    
}
