//
//  LocationsListCell.swift
//  code-challenge
//
//  Created by mahesh on 4/1/18.
//  Copyright © 2018 mahesh. All rights reserved.
//

import UIKit
import CoreData
class LocationsListCell: UITableViewCell {

    var location:Locations?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(_ location:Locations?){
        self.textLabel?.text = location?.formatted_address
        self.accessoryType = ((location?.saved.boolValue ?? false)) ? .checkmark : .none
    }
    func configureMapCell(string:String){
        self.textLabel?.text = string
        self.accessoryType = .disclosureIndicator
    }
}
