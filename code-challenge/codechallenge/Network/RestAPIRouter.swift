//
//  RestAPIRouter.swift
//  code-challenge
//
//  Created by mahesh on 3/31/18.
//  Copyright © 2018 mahesh. All rights reserved.
//

import Foundation
import Alamofire

public enum RestAPIRouter: URLRequestConvertible {
    case getLocations([String:Any])
    
    var method: HTTPMethod {
        switch self {
        case .getLocations(_):      return .get
        }
    }
    var path: String {
        switch self {
        case .getLocations(_):
            return "maps/api/geocode/json"
        }
    }
    public func asURLRequest() throws -> URLRequest {
        let url = RestAPIController.sharedInstance.baseUrl.appendingPathComponent(self.path)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        print("======Request Details======")
        print("---------URL--------- \n \(url.absoluteString)")
        switch self {
        case .getLocations(let params):
            print("---------Params--------- \n \(params)")
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: params)
            print("======End Request Details======")
        }
        return urlRequest
    }
}



