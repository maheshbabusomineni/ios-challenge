//
//  RestAPIController.swift
//  code-challenge
//
//  Created by mahesh on 3/31/18.
//  Copyright © 2018 mahesh. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import EZLoadingActivity

internal var CurrentBaseUrl: URL = RestAPIController.defaultBaseUrl

class RestAPIController {
    public static let sharedInstance = RestAPIController()
    public static let defaultBaseUrl = URL(string: "https://maps.googleapis.com/")!
    
    public var baseUrl: URL {
        get {
            return CurrentBaseUrl
        }
        set {
            CurrentBaseUrl = newValue
        }
    }
    //Private properties
    fileprivate var af: Alamofire.SessionManager
    
    public init() {
        let config = URLSessionConfiguration.default
        self.af = Alamofire.SessionManager(configuration: config)
    }
    
    /// Execute a network request with thes erver
    ///
    /// - Parameter request; URLRequset to make
    /// - Parameter completion: a completion block containing the rseponse data, JSON data (on success) and any error codes.  This completion block wil run on runQueue, the same DispatchQueue that the state machine execute son.
    class func startRequest(request:RestAPIRouter, completion: @escaping (_ response: ServerResponse)->Void = { _ in }) {
        
        EZLoadingActivity.show("Please wait", disableUI: true)
        let af = RestAPIController.sharedInstance.af
        
        //print("Server Request: \(method) \(url)")
        af.request(request)
            .validate(statusCode: 200..<600)
            .validate(contentType: ["application/json; charset=utf-8","text/plain"])
            .responseJSON { response in
                let serverResponse: ServerResponse
                
                switch response.result {
                case .success(let value ):
                    serverResponse = ServerResponse(json: JSON(value),
                                                    error: nil,
                                                    response: response)
                case .failure(let error):
                    serverResponse = ServerResponse(json: nil,
                                                    error: error,
                                                    response: response)
                    
                    let body: String
                    if let data = response.data {
                        body = String(data: data, encoding: .utf8)!
                    } else {
                        body = "(no body)"
                    }
                    print("Server ERROR: \(error.localizedDescription)\n\(body)\n")
                }
                
                EZLoadingActivity.hide()
                completion(serverResponse)
        }
    }
    class func startStringRequest(request:RestAPIRouter, completion: @escaping (_ response: ServerResponse)->Void = { _ in }) {
        
        let af = RestAPIController.sharedInstance.af
        
        //print("Server Request: \(method) \(url)")
        af.request(request)
            .validate(statusCode: 200..<600)
            .validate(contentType: ["application/json; charset=utf-8","text/plain"])
            .responseString { response in
                let serverResponse: ServerResponse
                
                switch response.result {
                case .success(let value ):
                    print("------Response-------\n \(JSON(value))")
                    serverResponse = ServerResponse(json: JSON(value),
                                                    error: nil,
                                                    response: nil)
                case .failure(let error):
                    serverResponse = ServerResponse(json: nil,
                                                    error: error,
                                                    response: nil)
                    
                    let body: String
                    if let data = response.data {
                        body = String(data: data, encoding: .utf8)!
                    } else {
                        body = "(no body)"
                    }
                    print("Server ERROR: \(error.localizedDescription)\n\(body)\n")
                }
                
                completion(serverResponse)
        }
    }
}
public struct ServerResponse {
    let json: JSON?
    let error: Error?
    let response: DataResponse<Any>?
}
public enum ServerError: Error {
    case generalError(String)
}

// MARK: Notification Events
extension RestAPIController {
    enum NotificationMetaKey : String {
        case url
    }
}
extension Notification.Name {
    static let serviceUrlDidChange = Notification.Name("service.url.didChange")
}



