//
//  MapViewExtension.swift
//  code-challenge
//
//  Created by mahesh on 4/1/18.
//  Copyright © 2018 mahesh. All rights reserved.
//

import UIKit
import MapKit
extension MKMapView {
    func zoomToFitMapAnnotations() {
        guard self.annotations.count > 0 else {
            return
        }
        var topLeftCoord: CLLocationCoordinate2D = CLLocationCoordinate2D()
        topLeftCoord.latitude = -90
        topLeftCoord.longitude = 180
        var bottomRightCoord: CLLocationCoordinate2D = CLLocationCoordinate2D()
        bottomRightCoord.latitude = 90
        bottomRightCoord.longitude = -180
        for annotation: MKAnnotation in self.annotations {
            topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude)
            topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude)
            bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude)
            bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude)
        }
        
        var region: MKCoordinateRegion = MKCoordinateRegion()
        region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5
        region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5
        region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.4
        region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.4
        region = self.regionThatFits(region)
        self.setRegion(region, animated: false)
    }

    func centerMapOnLocation(location:CLLocation, regionRadius:Double) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        self.setRegion(coordinateRegion, animated: false)
    }
    func nearest(in objects:[MapsScene.Annotation], from userLocation:CLLocation) -> MapsScene.Annotation? {
        let allLocations = objects.map{$0.location}
        let nearestShopLocation = allLocations.min(by:{$0.distance(from: userLocation) < $1.distance(from: userLocation) })
        let nearestShops = objects.filter({$0.location == nearestShopLocation})
        return nearestShops.first
    }
}



