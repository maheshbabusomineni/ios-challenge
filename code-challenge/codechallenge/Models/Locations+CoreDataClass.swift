//
//  Locations+CoreDataClass.swift
//  
//
//  Created by mahesh on 3/31/18.
//
//

import Foundation
import CoreData
import SwiftyJSON

@objc(Locations)
public class Locations: NSManagedObject {
    class func createLocation(_ json:JSON, completionHandler: @escaping EmptyCompletionHandler) -> (){
        
        // Create Managed Object
        let context = Constants.managedObjectContext
        let entityDescription = NSEntityDescription.entity(forEntityName: Constants.CoreDataEntities.Locations.rawValue as String, in: context)
        
        //Create new entity
        let newEntity:Locations = NSManagedObject(entity: entityDescription!, insertInto: context) as! Locations
        if let value = json["formatted_address"].string { newEntity.formatted_address  = value }
        if let location = json["geometry"].dictionary!["location"] {
            if let value = location["lat"].double { newEntity.lat  = value }
            if let value = location["lng"].double { newEntity.lng  = value }
        }
        //Save the object
        newEntity.saved = NSNumber(value: false)
        do {
            try newEntity.managedObjectContext?.save()
        } catch {
            print(error)
        }
        completionHandler()
    }
    class func fetchLocations() -> NSFetchedResultsController<Locations>? {
        
        let context = Constants.managedObjectContext
        let fetchRequest: NSFetchRequest<Locations> = Locations.fetchRequest()
        let sortDescriptor : NSSortDescriptor  = NSSortDescriptor(key: "formatted_address", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        do {
            try aFetchedResultsController.performFetch()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        return aFetchedResultsController
    }
    class func truncateLocationsData(completionHandler: @escaping EmptyCompletionHandler){
        
        let context = Constants.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:Constants.CoreDataEntities.Locations.rawValue)
        fetchRequest.predicate = NSPredicate(format: "saved == %@", NSNumber(value: false))
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try (((UIApplication.shared.delegate as! AppDelegate).persistentContainer).persistentStoreCoordinator).execute(deleteRequest, with: context)
        } catch let error as NSError {
            // TODO: handle the error
            print(error.description)
        }
        completionHandler()
    }

}
