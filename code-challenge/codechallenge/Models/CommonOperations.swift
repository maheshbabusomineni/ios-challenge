//
//  CommonOperations.swift
//  SwiftCoreDataSimpleDemo
//
//  Created by maheshbabu.somineni on 12/11/15.
//  Copyright © 2018 mahesh. All rights reserved.
//

import UIKit
import CoreData

enum ConditionType:Int {
    case and = 0
    case or
}

class CommonOperations: NSObject {
    class func fetchAllObjects(entityName:String, context:NSManagedObjectContext) -> [Any]? {
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: context)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        do {
            
            let result = try context.fetch(fetchRequest)
            return result
            
        } catch {
            
            print(error as NSError)
            return nil
        }
    }
    class func fetchWithParams(entityName:String, params:[String:Any], condition:ConditionType, context:NSManagedObjectContext) -> [Any]? {
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: context)
        
        var subPredicates : [NSPredicate] = []
        
        for key in params.keys{
            
            if let value = params[key] as? Int {
                let subPredicate = NSPredicate(format: "%K == %i", key,Int32(value))
                subPredicates.append(subPredicate)
            }
            if let value = params[key] as? String {
                let subPredicate = NSPredicate(format: "%K == %@", key,value)
                subPredicates.append(subPredicate)
            }
        }
        switch condition {
        case .and:
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: subPredicates)
        default:
            fetchRequest.predicate = NSCompoundPredicate(orPredicateWithSubpredicates: subPredicates)
        }

        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        do {
            
            let result = try context.fetch(fetchRequest)
            return result 
            
        } catch {
            print(error as NSError)
            return nil
        }
    }
   
    class func truncateAllObjects(entityName:String, context:NSManagedObjectContext) {
        //Fetch object
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        //Fetch request properties
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        fetchRequest.includesPropertyValues = false
        
        //Execute request
        do {
            if let resultsArray = try context.fetch(fetchRequest) as? [NSManagedObject] {
               
                for result in resultsArray {
                    
                    context.delete(result)
                }
                
                try context.save()
            }
        } catch {
            
            print(error as NSError)
        }
    }
}
