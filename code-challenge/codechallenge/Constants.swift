//
//  Constants.swift
//  code-challenge
//
//  Created by mahesh on 3/31/18.
//  Copyright © 2018 mahesh. All rights reserved.
//

import UIKit
struct Constants {
    
    static let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    static let appName = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as! String
    
    enum CoreDataEntities : String{
        case Locations = "Locations"
    }
    
    enum ViewControllers:String {
        case LocationsListViewController
    }
    enum StoryboardNames:String {
        case LocationsList
    }
}


