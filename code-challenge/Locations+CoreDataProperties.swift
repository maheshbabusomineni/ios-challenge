//
//  Locations+CoreDataProperties.swift
//  
//
//  Created by mahesh on 3/31/18.
//
//

import Foundation
import CoreData


extension Locations {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Locations> {
        return NSFetchRequest<Locations>(entityName: "Locations")
    }

    @NSManaged public var formatted_address: String?
    @NSManaged public var lat: Double
    @NSManaged public var lng: Double

}
