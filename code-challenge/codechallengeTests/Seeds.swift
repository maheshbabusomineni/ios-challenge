//
//  Seeds.swift
//  codechallengeTests
//
//  Created by mahesh on 4/1/18.
//  Copyright © 2018 mahesh. All rights reserved.
//

@testable import Pods_codechallenge
import XCTest

struct Seeds{
    struct Reqest {
        var address = "Sprintfield"
        var sensor = false
    }
    struct Location {
        var name:String
        var lat:Double
        var lng:Double
    }
    struct Locations{
        static let loation1 = Location.init(name: "Springfield, MO, USA", lat:37.2089572 , lng: -93.29229889999999)
        static let loation2 = Location.init(name: "Springfield, MA, USA", lat:42.1014831 , lng: -72.589811)
        static let loation3 = Location.init(name: "Springfield, IL, USA", lat:39.78172130000001 , lng: -89.6501481)
        static let allLocations = [Locations.loation1,Locations.loation2, Locations.loation3]
    }
}
