//
//  LocationsListWorkerTests.swift
//  codechallengeTests
//
//  Created by mahesh on 4/1/18.
//  Copyright © 2018 mahesh. All rights reserved.
//

import XCTest
@testable import codechallenge
class LocationsListWorkerTests: XCTestCase {
    var sut: LocationsWorker!
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.sut = LocationsWorker()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFetchingApiData() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let expectations = expectation(description: "Waiting for locations data")
        let params = ["address":Seeds.Reqest().address, "sensor":Seeds.Reqest().sensor] as [String : Any]
        RestAPIController.startRequest(request:.getLocations(params) ){ (response) in
            XCTAssertNotNil(response.json)
            expectations.fulfill()
        }
        waitForExpectations(timeout:20) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
