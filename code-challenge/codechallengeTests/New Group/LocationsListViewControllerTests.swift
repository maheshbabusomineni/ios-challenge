//
//  LocationsListViewControllerTests.swift
//  codechallengeTests
//
//  Created by mahesh on 4/1/18.
//  Copyright © 2018 mahesh. All rights reserved.
//

import XCTest
@testable import codechallenge
class LocationsListViewControllerTests: XCTestCase {
    var viewController:LocationsListViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.viewController = LocationsListViewController.getInstance()
        UIWindow().addSubview(self.viewController.view)
        RunLoop.current.run(until: Date())
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTableViewLoaded() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertNotNil(self.viewController.tableView)
        XCTAssertNotNil(self.viewController.tableView.delegate)
        XCTAssertNotNil(self.viewController.tableView.dataSource)
        XCTAssertNotNil(self.viewController.tableView.tableHeaderView)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
